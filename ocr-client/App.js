import React from 'react';
import { KeyboardAvoidingView, ActivityIndicator, ImageBackground, Alert,
    Button, Image, View, Text, TouchableOpacity, TouchableHighlight, Platform, StyleSheet} from 'react-native';
import { ImagePicker, Camera, Permissions, Constants} from 'expo';
import { validateEmotions, validateMatch } from './components/faceCompare';
import { Ionicons } from '@expo/vector-icons';
import { SERVICES, uploadToServer } from './components/utils';
import Form from "./components/form";
import PDFReader from 'rn-pdf-reader-js';


export default class App extends React.Component { 

  constructor(props) {
    super(props);
    console.disableYellowBox = true;
    this.EmotionsEnum = ["happiness", "surprise", "anger"];
    this.emotion = Math.floor(Math.random() * this.EmotionsEnum.length);
    this.verifText = "Verifying emotion..."
    this.validateIdentity = this.validateIdentity.bind(this);
    this.state = {
        cameraOn: false,
        step: 0,
        scanning: false,
        image: null,
        hasCameraPermission: null,
        type: Camera.Constants.Type.back,
        faceid: null,
        emotionOk: null,
        similarityOk: null,
        nume: "",
        prenume: "",
        adresa: "",
        cnp: "",
        serie: "",
        nr: "",
        pozaCI: "",
        pdf: "",
        email: "",
        error: ""
    };
    this.buletin = null;
    this.nextStep = this.nextStep.bind(this);
    this.scanIC = this.scanIC.bind(this);
    this.renderCamera = this.renderCamera.bind(this);
    this.takeAndValidate = this.takeAndValidate.bind(this);
    this.alertChoosePhotoMethod = this.alertChoosePhotoMethod.bind(this);
    this.toggleCamera = this.toggleCamera.bind(this);
    this.chooseImage = this.chooseImage.bind(this);
    this.fillFields = this.fillFields.bind(this);
    this.renderLastScreen = this.renderLastScreen.bind(this);
    this.reset = this.reset.bind(this);
    this.checkFieldsFilled = this.checkFieldsFilled.bind(this);
  }

  nextStep() {
    if (this.step == 0 && !this.checkFieldsFilled()) {
      this.setState({
        error: "All fields are mandatory"
      })
      return;
    }
    this.setState(prevState => {
      return {
        step: prevState.step+1
      };
    })
  }

  toggleCamera() {
    this.setState(prevState => {
      return {
        cameraOn: !prevState.cameraOn
      }
    })
  }

  checkFieldsFilled() {
    return this.state.nume != "" &&
        this.state.prenume != "" &&
        this.state.adresa != "" &&
        this.state.cnp != "" &&
        this.state.serie != "" &&
        this.state.nr != "" &&
        this.state.email != "";
  }

  reset() {
    this.setState({
      cameraOn: false,
      step: 0,
      scanning: false,
      image: null,
      hasCameraPermission: null,
      type: Camera.Constants.Type.back,
      faceid: null,
      emotionOk: null,
      similarityOk: null,
      nume: "",
      prenume: "",
      adresa: "",
      cnp: "",
      serie: "",
      nr: "",
      pozaCI: "",
      pdf: ""
    });
  }

  renderLastScreen() {
    return  (<View style={styles.lastContainer}>
      <PDFReader
          source={{ uri: "http://192.168.100.57:8080" + this.state.pdf }}
      />
      <TouchableOpacity onPress={this.reset}>
        <View style={ {flexDirection: "row", backgroundColor: "steelblue"}}>
            <Text style={{alignItems: 'center', fontSize: 18,color:"white", padding:18, paddingRight:4, paddingLeft: 10}}>Contract completed! Check your mail!</Text>
            <Ionicons name={"ios-mail"} size={32} color="white" style={{paddingTop:14}}/>
        </View>
      </TouchableOpacity>
  </View>
  );
  }

  fillFields(image) {
    console.log("Scanning IC");
    this.setState(prevState => {
      return {
        scanning: true,
      };
    });
    this.buletin = image;
    uploadToServer(SERVICES.ocr, image).then((result) => {
      console.log(result);
      this.setState(prevState => {
        return {
          scanning: false,
          prenume: result.mrz.first_line.first_name1 + " " + result.mrz.first_line.first_name2,
          nume: result.mrz.first_line.last_name,
          adresa: result.address.join(" "),
          cnp: result.mrz.second_line.cnp,
          serie: result.mrz.second_line.seriesCode,
          nr: result.mrz.second_line.seriesNumber,
          pozaCI: result.files.face,
          pdf: result.files.pdf
        }
      })
    });
  }

  async chooseImage() {
    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
    });

    if (!result.cancelled) {
        this.fillFields(result)
    }
  }

  alertChoosePhotoMethod() {
    Alert.alert(
      'Upload method',
      'Choose desired upload method',
      [
        {text: 'Take Photo', onPress: () => this.toggleCamera() },
        {text: 'Choose from library', onPress: () => this.chooseImage()},
      ],
      { cancelable: true }
    )
  }

  renderUserValidation() {
    console.log("USER VALIDATION");
    return (
        <View style={
            { flex: 1, alignItems: 'center', justifyContent: 'center' }
        }>
            <View style={ {flexDirection: "row"}}>
            <Ionicons name={"ios-archive"} size={32} color="steelblue" style={{paddingRight:4}}/>
            <Button
                title="Pick an image from camera roll"
                onPress={this._pickImage}
            />
            </View>

            <TouchableHighlight
                style ={{
                    margin : 4
                }}>
            <View style={ {flexDirection: "row"}}>
            <Ionicons name={"ios-camera"} size={32} color="steelblue" style={{paddingRight:4}} />
            <Button
                title="Take a photo"
                onPress={this.toggleCamera}
            />
            </View></TouchableHighlight>

            <Text style={{margin:4, fontSize: 17, fontWeight: 'bold',color:"white"}}>Select/Take photo with emotion: {this.EmotionsEnum[this.emotion]}!</Text>             
             {this.state.image &&
            <Image source={{ uri: this.state.image }} style={{ width: 200, height: 200, margin: 4 }} />}

    <View style={ {flexDirection: "row", justifyContent: 'space-around'}}>
            {this.state.image && (this.state.emotionOk == null) && <Text style={[styles.textStyle]}>{this.verifText}</Text>}
            {this.state.image && (this.state.emotionOk == null) && <ActivityIndicator size="small" color="white" />}
            </View>

            {(this.state.emotionOk != null) && (this.state.emotionOk == true ? <Text style={{color:"green", fontSize:18, fontWeight: 'bold'}}>Emotion Verified!</Text>
                : <Text style={{color:"red", fontWeight: 'bold', fontSize:18}}>Emotion failed... try again!</Text>)}
            <View style={ {flexDirection: "row", justifyContent: 'space-around'}}>
            {this.state.emotionOk && (this.state.similarityOk == null) && <Text style={{fontSize:18}}>Verifying similarity...</Text>}
            {this.state.emotionOk && (this.state.similarityOk == null) && <ActivityIndicator size="small" color="gray" />}
            </View>
            {this.state.emotionOk  && (this.state.similarityOk != null)&& (this.state.similarityOk == true ? <Text style={{color:"green", fontSize:18, fontWeight: 'bold'}}>Similarity Verified!</Text>
                : <Text style={{color:"red", fontSize:14}}>Similarity failed... try again!</Text>)}
            {this.state.emotionOk && this.state.similarityOk}
        </View>
    );
  }

  async scanIC() {

    if (this.camera) {
      let photo = await this.camera.takePictureAsync();

      this.setState({
          cameraOn: false,
          image: photo.uri
      });

      this.fillFields(photo);
    }
    
  }

  async validateIdentity(image) {
    var blobImage = await (await fetch(image.uri)).blob();
     //console.log(blobImage);
     console.log(this.buletin);
     var blobImage1 = await (await fetch(this.buletin.uri)).blob();
     //console.log(blobImage);
    validateEmotions(this.EmotionsEnum[this.emotion], blobImage).then((obj) => {
        if (obj.result) {
            console.log("Gata emotia");
            this.setState(prevState => {
                return {
                    emotionOk: true
                };
            });

            validateMatch(blobImage1, obj.faceID).then((result) => {
                this.setState(prevState => {
                    return {
                        similarityOk: result
                    };
                });
                console.log("Gata matchu");
                console.log(result);
            });
        }
        else {  
            this.setState(prevState => {
                return {
                    emotionOk: false
                };
            });
        }
        //console.log("Naspa");
    });
}

  async takeAndValidate() {
    if (this.camera) {
        let photo = await this.camera.takePictureAsync();

        this.setState({
            cameraOn: false,
            image: photo.uri
        });

        this.setState(prevState => {
            return {
                emotionOk: null
            };
        });

        this.setState(prevState => {
            return {
                similarityOk: null
            };
        });

        this.validateIdentity(photo);
    }
  }

  async componentWillMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === 'granted' });
  }

  renderCamera() {
    const { hasCameraPermission } = this.state;
    if (hasCameraPermission === null) {
        return (<View />);
    } else if (hasCameraPermission === false) {
        return (<Text>No access to camera</Text>);
    } else
    return (
      <View style={{ flex: 1 }}>
      <Camera style={{ flex: 1 }} type={this.state.type} ref={ref => { this.camera = ref; }}>
          <View
              style={{
                  flex: 1,
                  backgroundColor: 'transparent',
                  flexDirection: 'row',
              }}>
              <TouchableOpacity
                  style={{
                      flex: 1,
                      alignSelf: 'flex-end',
                      alignItems: 'flex-start',
                      marginLeft: 7
                  }}
                  onPress={() => {
                      this.setState({
                          type: this.state.type === Camera.Constants.Type.back
                              ? Camera.Constants.Type.front
                              : Camera.Constants.Type.back,
                      });
                  }}>
                  <Text
                      style={{ fontSize: 18, marginBottom: 10, color: 'white' }}>
                      {' '}Flip{' '}
                  </Text>
              </TouchableOpacity>
              <TouchableOpacity
                  style={{
                      flex: 1,
                      alignSelf: 'flex-end',
                      alignItems: 'flex-end',
                      marginRight: 7
                  }}
                  onPress= {this.state.step == 0? this.scanIC: this.takeAndValidate}>
                  <Text
                      style={{ fontSize: 18, marginBottom: 10, color: 'white' }}>
                      {' '}Take{' '}
                  </Text>
              </TouchableOpacity>

          </View>
      </Camera>
  </View>
    );
  }

  _pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: true,
        aspect: [4, 3],
    });

    console.log(result);

    if (!result.cancelled) {
        this.setState({ image: result.uri });
        this.setState(prevState => {
            return {
                emotionOk: null
            };
        });

        this.setState(prevState => {
            return {
                similarityOk: null
            };
        });
        this.validateIdentity(result);
    }

};

  render() {
    return this.state.cameraOn ?
            this.renderCamera():
            this.state.step > 1?
            this.renderLastScreen():
        (
        <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
          {/* <Text>Open up App.js to start working on your app! Step what {this.state.step}</Text> */}
          <ImageBackground source={require('./assets/image2.jpg')} resizeMethod="resize" resizeMode="cover" style={styles.bgImage}>
            <View style={styles.outerImage}>
                <Image style={styles.image} 
                  source={require("./assets/vodafone_logo.png")} resizeMode="cover" resizeMethod="resize"></Image>
            </View>
            <View style={styles.content}>
              {this.state.step == 0 ?
                this.state.scanning ?
                <ActivityIndicator size="large" color="white" style={{flex:1, alignItems:"center", justifyContent:"center"}}/>
                : <View>
                  <Form nume={this.state.nume} prenume={this.state.prenume} cnp={this.state.cnp} serie={this.state.serie} nr={this.state.nr} adresa={this.state.adresa} pozaCI={this.state.pozaCI}/> 
                  {this.state.error != "" ? <Text color="red">{this.state.error}</Text>: null}
                  </View>
              : this.state.step == 1 ?
                this.renderUserValidation()
              : <Text>N-avem</Text>}
            </View>
            <View style={styles.buttonGroup}>
              { this.state.step == 0 &&
              <View style={styles.button}>
                <Button
                    color="orange"
                    title='Scan IC'
                    onPress={this.alertChoosePhotoMethod}
                />
              </View>
              }
              { this.state.step < 3 &&
              <View style={styles.button}>
                <Button
                    color="blue"
                    title='Next step'
                    onPress={this.nextStep}
                    // enabled={this.state.step == 1 && !this.state.similarityOk ? false: true}
                />
              </View>
              }
            </View>
          </ImageBackground>
        </KeyboardAvoidingView>
      );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: 'center',
    justifyContent: 'center',
  },
  content: {
    height: 400
  },
  button: {
    // alignItems: "stretch" 
    padding: 5,
    width: 100
  },
  buttonGroup: {
    flexDirection: 'row',
  },
  image: {
    // flex: 1,
    width: 200,
    height: 50
    // resizeMode: 'contain'
  },
  outerImage: {
    alignItems: "center"
  },
  bgImage: {
    flex: 1,
    width: 400,
    alignItems: "center",
    justifyContent: 'center',
  },
  lastContainer: {
    flex: 1,
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
}
});



