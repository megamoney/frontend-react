import React from 'react';
import { StyleSheet, Text, View, TextInput, Image, TouchableOpacity } from 'react-native';
import { ImagePicker, Camera, Permissions} from 'expo';

export const SERVICES = {
    ocr: "users/card/ocr"
}

export async function uploadToServer(service, image, email) {    
    var data = new FormData();
    console.log(image);
    data.append('card', {
        uri: image.uri,
        type: 'image/jpeg', // or photo.type
        name: 'testPhotoName'
    });
    data.append('email', "corneliu.dcv@gmail.com")
    return fetch('http://192.168.100.57:8080/users/card/ocr', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data
    }).then(async (result) => {
        return await result.json();
    }).catch(error => {
        console.log(error);
    });
}