import React from 'react';
import { ActivityIndicator, Button, Image, View, Text, TouchableOpacity, TouchableHighlight, Platform, StyleSheet} from 'react-native';
import { ImagePicker, Camera, Permissions} from 'expo';
import { validateEmotions, validateMatch } from './faceCompare';

export default class Camera extends React.Component {
    constructor(props) {
        this.state = {
            hasCameraPermission: null,
            type: Camera.Constants.Type.front,
            image: null
        };
    }

    render() {
        <View style={{ flex: 1 }}>
        <Camera style={{ flex: 1 }} type={this.state.type} ref={ref => { this.camera = ref; }}>
            <View
                style={{
                    flex: 1,
                    backgroundColor: 'transparent',
                    flexDirection: 'row',
                }}>
                <TouchableOpacity
                    style={{
                        flex: 1,
                        alignSelf: 'flex-end',
                        alignItems: 'flex-start',
                        marginLeft: 7
                    }}
                    onPress={() => {
                        this.setState({
                            type: this.state.type === Camera.Constants.Type.back
                                ? Camera.Constants.Type.front
                                : Camera.Constants.Type.back,
                        });
                    }}>
                    <Text
                        style={{ fontSize: 18, marginBottom: 10, color: 'white' }}>
                        {' '}Flip{' '}
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={{
                        flex: 1,
                        alignSelf: 'flex-end',
                        alignItems: 'flex-end',
                        marginRight: 7
                    }}
                    onPress= { async() => {
                        if (this.camera) {
                            let photo = await this.camera.takePictureAsync();

                            this.setState({
                                cameraOn: false,
                                image: photo.uri
                            });

                            this.setState(prevState => {
                                return {
                                    emotionOk: null
                                };
                            });

                            this.setState(prevState => {
                                return {
                                    similarityOk: null
                                };
                            });

                            this.validateIdentity(photo);
                        }
                    }}>
                    <Text
                        style={{ fontSize: 18, marginBottom: 10, color: 'white' }}>
                        {' '}Take{' '}
                    </Text>
                </TouchableOpacity>

            </View>
        </Camera>
    </View>
    }
}
