import React from 'react';
import { ActivityIndicator, Button, Image, View, Text, TouchableOpacity, TouchableHighlight, Platform, StyleSheet} from 'react-native';
import { ImagePicker, Camera, Permissions} from 'expo';
import { validateEmotions, validateMatch } from './faceCompare';
import { Ionicons } from '@expo/vector-icons';

export default class ImagePickerExample extends React.Component {
    constructor(props) {
        super(props);
        this.EmotionsEnum = ["happiness", "surprise", "anger", "sadness"];
        this.state = {
            image: null,
            hasCameraPermission: null,
            type: Camera.Constants.Type.front,
            cameraOn: false,
            faceid: null,
            emotionOk: null,
            similarityOk: null
        };
        this.changeCamera = this.changeCamera.bind(this);
        this._takePhoto = this._takePhoto.bind(this);
        this.emotion = Math.floor(Math.random() * this.EmotionsEnum.length);
        this.verifText = "Verifying emotion..."
        this.validateIdentity = this.validateIdentity.bind(this);
    }


    changeCamera(){
        console.log(this.state);
        this.setState(prevState => {
            return {
                cameraOn: !prevState.cameraOn
            }
        })
    }

    render() {
        let { image } = this.state;


        return this.state.cameraOn ?
            this._takePhoto():
            (
            <View style={
                { flex: 1, alignItems: 'center', justifyContent: 'center' }
            }>
                <View style={ {flexDirection: "row"}}>
                <Ionicons name={"ios-archive"} size={32} color="steelblue" style={{paddingRight:4}}/>
                <Button
                    title="Pick an image from camera roll"
                    onPress={this._pickImage}
                />
                </View>

                <TouchableHighlight
                    style ={{
                        margin : 4
                    }}>
                <View style={ {flexDirection: "row"}}>
                <Ionicons name={"ios-camera"} size={32} color="steelblue" style={{paddingRight:4}} />
                <Button
                    title="Take a photo"
                    onPress={this.changeCamera}
                />
                </View></TouchableHighlight>

                <Text style={{margin:4}}>Select/Take a photo of you {this.EmotionsEnum[this.emotion]}!</Text>
                {image &&
                <Image source={{ uri: this.state.image }} style={{ width: 300, height: 300, margin: 4 }} />}

                <View style={ {flexDirection: "row", justifyContent: 'space-around'}}>
                {image && (this.state.emotionOk == null) && <Text style={[styles.textStyle]}>{this.verifText}</Text>}
                {image && (this.state.emotionOk == null) && <ActivityIndicator size="small" color="gray" />}
                </View>

                {(this.state.emotionOk != null) && (this.state.emotionOk == true ? <Text style={{color:"green", fontSize:14}}>Emotion Verified!</Text>
                    : <Text style={{color:"red", fontSize:14}}>Emotion failed... try again!</Text>)}
                <View style={ {flexDirection: "row", justifyContent: 'space-around'}}>
                {this.state.emotionOk && (this.state.similarityOk == null) && <Text style={[styles.textStyle]}>Verifying similarity...</Text>}
                {this.state.emotionOk && (this.state.similarityOk == null) && <ActivityIndicator size="small" color="gray" />}
                </View>
                {this.state.emotionOk  && (this.state.similarityOk != null)&& (this.state.similarityOk == true ? <Text style={{color:"green", fontSize:14}}>Similarity Verified!</Text>
                    : <Text style={{color:"red", fontSize:14}}>Similarity failed... try again!</Text>)}
                {this.state.emotionOk && this.state.similarityOk}
            </View>
        );
    }

    async validateIdentity(image) {
        var blobImage = await (await fetch(image.uri)).blob();
         //console.log(blobImage);
        validateEmotions(this.EmotionsEnum[this.emotion], blobImage).then((obj) => {
            if (obj.result) {
                console.log("Gata emotia");
                this.setState(prevState => {
                    return {
                        emotionOk: true
                    };
                });
                validateMatch(blobImage, obj.faceID).then((result) => {
                    this.setState(prevState => {
                        return {
                            similarityOk: true
                        };
                    });
                    console.log("Gata matchu");
                    console.log(result);
                });
            }
            else {
                this.setState(prevState => {
                    return {
                        emotionOk: false
                    };
                });
            }
            //console.log("Naspa");
        });
    }

    _nextStep() {

    }

    _pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: true,
            aspect: [4, 3],
        });

        console.log(result);

        if (!result.cancelled) {
            this.setState({ image: result.uri });
            this.setState(prevState => {
                return {
                    emotionOk: null
                };
            });

            this.setState(prevState => {
                return {
                    similarityOk: null
                };
            });
            this.validateIdentity(result);
        }

    };

    async componentWillMount() {
        const { status } = await Permissions.askAsync(Permissions.CAMERA);
        this.setState({ hasCameraPermission: status === 'granted' });
    }

    _takePhoto()  {
        const { hasCameraPermission } = this.state;
        if (hasCameraPermission === null) {
            return (<View />);
        } else if (hasCameraPermission === false) {
            return (<Text>No access to camera</Text>);
        } else
        return (
            <View style={{ flex: 1 }}>
                <Camera style={{ flex: 1 }} type={this.state.type} ref={ref => { this.camera = ref; }}
                >
                    <View
                        style={{
                            flex: 1,
                            backgroundColor: 'transparent',
                            flexDirection: 'row',
                        }}>
                        <TouchableOpacity
                            style={{
                                flex: 1,
                                alignSelf: 'flex-end',
                                alignItems: 'flex-start',
                                marginLeft: 7
                            }}
                            onPress={() => {
                                this.setState({
                                    type: this.state.type === Camera.Constants.Type.back
                                        ? Camera.Constants.Type.front
                                        : Camera.Constants.Type.back,
                                });
                            }}>
                            <Text
                                style={{ fontSize: 18, marginBottom: 10, color: 'white' }}>
                                {' '}Flip{' '}
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={{
                                flex: 1,
                                alignSelf: 'flex-end',
                                alignItems: 'flex-end',
                                marginRight: 7
                            }}
                            onPress= { async() => {
                                if (this.camera) {
                                    let photo = await this.camera.takePictureAsync();

                                    console.log(photo);
                                    this.setState({
                                        cameraOn: false,
                                        image: photo.uri
                                    });

                                    this.setState(prevState => {
                                        return {
                                            emotionOk: null
                                        };
                                    });

                                    this.setState(prevState => {
                                        return {
                                            similarityOk: null
                                        };
                                    });

                                    this.validateIdentity(photo);
                                }
                            }}>
                            <Text
                                style={{ fontSize: 18, marginBottom: 10, color: 'white' }}>
                                {' '}Take{' '}
                            </Text>
                        </TouchableOpacity>

                    </View>
                </Camera>
            </View>
        );
    };
}

const styles = StyleSheet.create({
    buttonStyle: {
        color: 'red',
        marginTop: 20,
        padding: 20,
        backgroundColor: 'green'
    },
    textStyle : {
        fontSize: 14,
        padding: 10
    }
});