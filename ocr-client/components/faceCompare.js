const API_URL = "https://{region}.api.cognitive.microsoft.com/face/{version}/{service}";
const CONFIDENCE_THRESHOLD = 0.25;
const EMOTION_THRESHOLD = 0.4;
const DEFAULT_REQUESTINFO = {
	region: 'westcentralus',
	version: 'v1.0',
	subscriptionKey: '66916b5e07214b2d9c14ef0613659441',
};

const FACE_OK = 0;
const NO_EMOTION_MATCH = 1;
const NO_FACE_MATCH = 2;

const NO_FACE_DETECTED = 1;

function dictToURI(dict) {
	var str = [];
	for(var p in dict)
		str.push(encodeURIComponent(p) + "=" + encodeURIComponent(dict[p]));

	return str.join("&");
}

function faceCompareAPICall(requestInfo, referenceFaceID, faceID) {
	// ensure correct service, regardless of caller intention
	requestInfo.service = "findsimilars";
	url = 'https://westcentralus.api.cognitive.microsoft.com/face/v1.0/findsimilars';
	return fetch(url, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Ocp-Apim-Subscription-Key': requestInfo.subscriptionKey,
		},
		body: JSON.stringify({
			'faceId': faceID,
			'faceIds': [ referenceFaceID ],
			'maxNumOfCandidatesReturned': 1,
			'mode': 'matchFace',
		}),
	});
}

function faceDetectAPICall(requestInfo, params, image) {
	// ensure correct service, regardless of caller intention
	requestInfo.service = "detect";
	url = 'https://westcentralus.api.cognitive.microsoft.com/face/v1.0/detect';
	url += '?' + dictToURI(params);
	return fetch(url, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/octet-stream',
			'Content-Length': image.length,
			'Ocp-Apim-Subscription-Key': requestInfo.subscriptionKey,
		},
		body: image,
	});
}

function detectFace(params, image) {
	let requestInfo = DEFAULT_REQUESTINFO;
	return faceDetectAPICall(requestInfo, params, image)
		.then((response) => {
			if (!response.ok)
				throw Error(response.statusText);

			return response.json();
		});
}

function getFaceID(image) {
	let params = {
		returnFaceId: true,
		returnFaceLandmarks: false,
		returnFaceAttributes: [ ],
	};

	return detectFace(params, image)
		.then((responseJSON) => {
			if (!responseJSON instanceof Array)
				throw Error('Face detection JSON malformed!');

			if ('faceId' in responseJSON[0])
				return responseJSON[0].faceId;
			else {
				Promise.reject({
					message: 'No face detected!',
					code: NO_FACE_DETECTED
				});
			}
		})
}

function getFaceIDAndEmotions(image) {
	let params = {
		returnFaceId: true,
		returnFaceLandmarks: false,
		returnFaceAttributes: [ 'emotion' ],
	};

	return detectFace(params, image)
		.then((responseJSON) => {
			if (!responseJSON instanceof Array)
				throw Error('Face detection JSON malformed!');

			if (responseJSON.length < 1)
				Promise.reject({
					message: 'No face detected!',
					code: NO_FACE_DETECTED
				});

			if ('faceId' in responseJSON[0]
				&& 'faceAttributes' in responseJSON[0]
				&& 'emotion' in responseJSON[0].faceAttributes)
				return {
					faceID: responseJSON[0].faceId,
					emotion: responseJSON[0].faceAttributes.emotion
				};
			else
				throw Error('Face detection JSON malformed!');
		})
}

function compareFaces(referenceFaceID, faceID) {
	let requestInfo = DEFAULT_REQUESTINFO;
	return faceCompareAPICall(requestInfo, referenceFaceID, faceID)
		.then((response) => {
			if (!response.ok) {
				throw Error(response.statusText);
			}

			return response.json();
		});
}

function getMatchConfidence(referenceFaceID, faceID) {
	return compareFaces(referenceFaceID, faceID)
		.then((responseJSON) => {
			if (!(responseJSON instanceof Array))
				throw Error('Face compare JSON malformed!');

			if (responseJSON.length < 1)
				Promise.reject({
					message: 'No face detected!',
					code: NO_FACE_DETECTED
				});

			if ('confidence' in responseJSON[0])
				return responseJSON[0].confidence;
			else
				throw Error('JSON contains no \'confidence\' value');
		})
}

function matchFaces(referenceFaceID, faceID) {
	return getMatchConfidence(referenceFaceID, faceID).then((confidence) => {
		console.log(confidence);
		return confidence >= CONFIDENCE_THRESHOLD;
	});
}

function checkEmotions(referenceEmotion, emotions) {
	if (referenceEmotion in emotions)
		return emotions[referenceEmotion] >= EMOTION_THRESHOLD;
	else
		throw Error('Emotion ' + referenceEmotion + ' not valid!');
}

export function validateMatch(referenceImage, faceID) {
	return getFaceID(referenceImage).then((referenceFaceID) => {
		return matchFaces(referenceFaceID, faceID);
	});
}

export function validateEmotions(referenceEmotion, image) {
	return getFaceIDAndEmotions(image).then((obj) => {
		return {
			faceID: obj.faceID,
			result: checkEmotions(referenceEmotion, obj.emotion)
		};
	});
}