import React from 'react';
import { StyleSheet, Text, View, TextInput, Image, TouchableOpacity } from 'react-native';
import { ImagePicker } from "expo";
import { validateEmotions, validateMatch } from './faceCompare'
import { uploadToServer } from './utils';

export default class Form extends React.Component {
    constructor(props) {
        super(props);
        console.log(props);
        this.state = {
            nume: props.nume,
            prenume: props.prenume,
            adresa: props.adresa,
            cnp: props.cnp,
            serie: props.serie,
            numar: props.nr,
            pozaCI: props.pozaCI,
            email: props.email
        }
        this.pickImage = this.pickImage.bind(this);
        this.validateIdentity = this.validateIdentity.bind(this);
    }

    async validateIdentity(image) {
        var blobImage = await (await fetch(image.uri)).blob();
        console.log(blobImage);
        validateEmotions("neutral", blobImage).then((obj) => {
            if (obj.result) {
                console.log("Gata emotia");
                validateMatch(blobImage, obj.faceID).then((result) => {
                    console.log("Gata matchu");
                    console.log(result);
                });
            }
            //console.log("Naspa");
        });
    }



    async pickImage() {
        let result = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: true,
            aspect: [4, 3],
        });

        console.log(result);

        if (!result.cancelled) {
            this.setState({ image: result.uri });
        }

        this.makeRequest(result);
    };

    updateField() {

    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.outerImage}>
                    <TouchableOpacity onPress={this.pickImage}>
                        <Image style={styles.image} source={this.state.pozaCI != "" ? {uri: "http://192.168.100.57:8080"+ this.state.pozaCI} : require("../assets/placeholder.png")} resizeMode="cover" resizeMethod="resize" on></Image>
                    </TouchableOpacity>
                </View>
                <View style={styles.input}>
                    {/* <Text style={styles.text}>Nume:</Text> */}
                    <TextInput style={styles.text} placeholder=" Nume" underlineColorAndroid='#fff' onChangeText={(text) => {this.setState({nume: text})}}
                        value={this.state.nume}>
                        {/* {this.state.nume} */}
                    </TextInput>
                </View>
                <View style={styles.input}>
                    {/* <Text style={styles.text}>Prenume:</Text> */}
                    <TextInput style={styles.text} placeholder=" Prenume" underlineColorAndroid='#fff' onChangeText={(text) => {this.setState({prenume: text})}}
                        value={this.state.prenume}>
                        {/* {this.state.prenume} */}
                    </TextInput>
                </View>
                <View style={styles.input}>
                    {/* <Text style={styles.text}>Adresa:</Text> */}
                    <TextInput style={styles.text} placeholder=" Adresa" underlineColorAndroid='#fff' onChangeText={(text) => {this.setState({adresa: text})}}
                        value={this.state.adresa}>
                        {/* {this.state.adresa} */}
                    </TextInput>
                </View>
                <View style={styles.input}>
                    {/* <Text style={styles.text}>CNP:</Text> */}
                    <TextInput style={styles.text} placeholder=" CNP" underlineColorAndroid='#fff' onChangeText={(text) => {this.setState({cnp: text})}}
                        value={this.state.cnp}>
                        {/* {this.state.cnp} */}
                    </TextInput>
                </View>
                    <View style={{padding: 5,
                            marginLeft: 10,
                            alignItems: "stretch",
                            flexDirection: "row"}}>
                    {/* <Text style={styles.text}>SERIE SI NR:</Text> */}
                    <TextInput style={{
                                color: "black",
                                fontSize: 16,
                                backgroundColor: "white",
                                borderRadius: 4,
                                borderWidth: 0.8,
                                borderColor: '#d6d7da', flex: 1}}
                         placeholder=" Serie" underlineColorAndroid='#fff' onChange={(text) => {this.setState({serie: text})}}
                         value={this.state.serie}>
                        {/* {this.state.serie} */}
                    </TextInput>
                    <TextInput style={{
                                color: "black",
                                fontSize: 16,
                                backgroundColor: "white",
                                borderRadius: 4,
                                borderWidth: 0.8,
                                borderColor: '#d6d7da', flex:1}}
                        placeholder=" Numar" underlineColorAndroid='#fff' onChange={(text) => {this.setState({numar: text})}}
                        value={this.state.numar}>
                        {/* {this.state.numar} */}
                    </TextInput>
                </View>
                <View style={styles.input}>
                    {/* <Text style={styles.text}>EMAIL:</Text> */}
                    <TextInput style={styles.text} placeholder=" Email" underlineColorAndroid='#fff' onChange={(text) => {this.setState({email: text})}}
                        value={this.state.email}>
                        {/* {this.state.email} */}
                    </TextInput>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        alignItems: "stretch",
        padding: 20,
        // borderRadius: 4,
        // borderWidth: 0.8,
        // borderColor: '#d6d7da',
    },
    text: {
        color: "black",
        fontSize: 16,
        // fontWeight: "bold",
        backgroundColor: "white",
        borderRadius: 4,
        borderWidth: 0.8,
        borderColor: '#d6d7da',
        // flex: 1
        // width: 280,
    },
    title: {
        fontSize: 19,
        fontWeight: 'bold',
    },
    activeTitle: {
        color: 'red', 
    },
    image: {
        // flex: 1,
        width: 100,
        height: 100,
        // resizeMode: 'contain'
        borderRadius: 4,
        borderWidth: 0.8,
        borderColor: '#d6d7da',
    },
    input: {
        // flex: 1,
        // flexDirection: "row",
        padding: 5,
        marginLeft: 10,
        width: 280,
        alignItems: "stretch",
    },
    outerImage: {
        alignItems: "center",
        marginBottom: 10
    }
  });